FROM alpine:3.4

RUN apk add --no-cache --update wget bash certbot ca-certificates

WORKDIR /opt/certbot

COPY run.sh ./run.sh
COPY wait_and_renew.sh ./wait_and_renew.sh

EXPOSE 80

CMD ["./wait_and_renew.sh"]
