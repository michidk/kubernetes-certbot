#!/bin/bash

set -eu

echo "running certbot on domains $DOMAINS"

readonly DOMAIN_MAIN=$(echo $DOMAINS | sed 's/,.*//')
readonly STAGING=${STAGING:-}

if [ -z "$STAGING" ]; then
  echo "requesting live cert"
else
  echo "requesting staging cert"
fi

echo "Generating certificate ${DOMAIN_MAIN}"
certbot \
  --non-interactive \
  --agree-tos \
  --standalone \
  --standalone-supported-challenges http-01 \
  --email "${LETS_ENCRYPT_EMAIL}" \
  --domains "${DOMAINS}" \
  ${STAGING:+"--staging"} \
  certonly

echo "Copying certs to output"
mkdir -p /output/certs/
cp -Lr /etc/letsencrypt/live/* /output/certs/
chmod -R 777 /output/certs

echo "done"